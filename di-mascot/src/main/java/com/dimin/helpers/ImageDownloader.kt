package com.dimin.helpers

import android.content.Context
import android.graphics.Bitmap
import android.os.AsyncTask
import android.os.Environment
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy
import com.bumptech.glide.request.RequestOptions
import java.io.File
import java.io.FileOutputStream
import java.lang.ref.WeakReference

class ImageDownloader (context: Context, imageName: String) : AsyncTask<String, Unit, Unit>() {
    private var mContext: WeakReference<Context> = WeakReference(context)

    private var mImageName = imageName

    val path = Environment.getExternalStoragePublicDirectory(
        Environment.DIRECTORY_DOCUMENTS)


    override fun doInBackground(vararg params: String?) {
        val url = params[0]
        val requestOptions = RequestOptions().override(100)
            .downsample(DownsampleStrategy.CENTER_INSIDE)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.NONE)

        mContext.get()?.let {
            val bitmap = Glide.with(it)
                .asBitmap()
                .load(url)
                .apply(requestOptions)
                .submit()
                .get()

            try {
                var file = File(path.absolutePath + "/texture")
                if (!file.exists()) {
                    file.mkdir()
                } else {
                    file.delete()
                }
                file = File(file, mImageName)
                val out = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.PNG, 85, out)
                out.flush()
                out.close()
                Log.i("Seiggailion", "Image saved."+ out)
            } catch (e: Exception) {
                Log.i("Seiggailion", "Failed to save image.")
            }
        }
    }
}