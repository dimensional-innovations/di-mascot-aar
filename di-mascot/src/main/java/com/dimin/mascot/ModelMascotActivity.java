package com.dimin.mascot;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.dimin.mascot.service.Config;
import com.dimin.mascot.service.ui.ThemedButton;
import com.dimin.util.android.ContentUtils;
import com.squareup.picasso.Picasso;

import org.imaginativeworld.whynotimagecarousel.CarouselItem;
import org.imaginativeworld.whynotimagecarousel.ImageCarousel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ModelMascotActivity extends AppCompatActivity {


    protected View background;
    protected View mLayout;
    protected ThemedButton usePhotoButton;
    protected ThemedButton backButton;
    protected ImageView sponsorLogo;
    protected RelativeLayout sponsorModelView;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ContentUtils.setThreadActivity(this);
        mLayout = getLayoutInflater().inflate(R.layout.activity_model_mascot_preview, null);
        setContentView(R.layout.activity_model_mascot_preview);

        Log.e("back", Config.Settings.INSTANCE.getBack());

        setSchool();
        addButtons();

    }

    private void setSchool() {
        ImageCarousel carousel = findViewById(R.id.carousel);
        List<CarouselItem> list = new ArrayList<>();

        for(int i = 0; i<Config.Mascot.gifs.toArray().length; i++){
            list.add(
                    new CarouselItem(
                            Config.Mascot.gifs.get(i),
                            Config.Mascot.labels.get(i)
                    )
            );
        }

        carousel.addData(list);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.out.println("Back pressed");
        Intent intent = null;
        try {
            intent = new Intent(this,  Class.forName(Objects.requireNonNull(Config.Settings.INSTANCE.getBack())) );
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        startActivity(intent);
        finish();
    }

    protected void addButtons() {

        sponsorModelView = findViewById(R.id.sponsorModelView);
        sponsorModelView.setBackgroundColor(Config.Colors.INSTANCE.getPrimary_color());

        sponsorLogo = findViewById(R.id.sponsor_logo);
        String sponsorImageUrl = Config.Images.INSTANCE.getSponsor_image_url();
        if (sponsorImageUrl.isEmpty()) {
            sponsorImageUrl = "https://sidearmsports.com/wp-content/uploads/2018/05/sidearm_logo_2-1.png";
        }
        Picasso.get().load(sponsorImageUrl).into(sponsorLogo);

        sponsorLogo.setOnClickListener(v -> {
            String sponsorLinkUrl = Config.Images.INSTANCE.getSponsor_link_url();
            if (sponsorLinkUrl.isEmpty()) {
                sponsorLinkUrl = "https://sidearmsports.com/";
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sponsorLinkUrl));
            startActivity(intent);
        });

        backButton = findViewById(R.id.backButton);

        backButton.setOnClickListener(v -> {
            onBackPressed();
        });

        usePhotoButton = findViewById(R.id.use_photo_button);
        usePhotoButton.setOnClickListener(v -> {
                    ImageCarousel carousel = findViewById(R.id.carousel);
                    int spot = carousel.getCurrentPosition();
                    String objURL = Config.Mascot.gltf.get(spot);
                    Intent sceneViewerIntent = new Intent(Intent.ACTION_VIEW);
                    Uri intentUri =
                            Uri.parse("https://arvr.google.com/scene-viewer/1.0").buildUpon()
                                    .appendQueryParameter("file", objURL)
                                    .appendQueryParameter("mode", "ar_only")
                                    .build();
                    sceneViewerIntent.setData(intentUri);
                    sceneViewerIntent.setPackage("com.google.ar.core");
                    startActivity(sceneViewerIntent);
        });
    }
}