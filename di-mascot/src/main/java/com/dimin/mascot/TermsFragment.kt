package com.dimin.mascot

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import androidx.core.app.ActivityCompat
import androidx.core.content.edit
import androidx.fragment.app.*
import androidx.navigation.fragment.findNavController
import com.dimin.mascot.service.Config


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class TermsFragment : Fragment() {

    var CAMERA_PERMISSION = Manifest.permission.CAMERA
    var RECORD_AUDIO_PERMISSION = Manifest.permission.RECORD_AUDIO
    var RC_PERMISSION = 101
    var allPermissions = arrayOf(
        CAMERA_PERMISSION,
        RECORD_AUDIO_PERMISSION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.FOREGROUND_SERVICE,
        Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION
    )
    var isTermsAccepted: Boolean = false
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_terms, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        var checkBox = view.findViewById<CheckBox>(R.id.terms_check_box)
        checkBox.buttonTintList = ColorStateList.valueOf(Config.Colors.primary_color)


        requestPermissions()
        view.findViewById<Button>(R.id.button_terms).setOnClickListener {
            if (checkBox.isChecked) {
                isTermsAccepted = true
                Config.userDefaults?.edit(true) {
                    println(isTermsAccepted)
                    isTermsAccepted?.let { it1 -> putBoolean("termsAccepted", it1)
                    println(it1)}
                }

                findNavController().navigate(R.id.action_TermsFragment_to_HomeFragment)

            }
        }
    }

    fun moveToNextView() {

    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            allPermissions,
            RC_PERMISSION
        )
    }

    private fun checkPermissions(): Boolean {
        var isGranted:Boolean = true
        for (permission in allPermissions) {
            if(isGranted) {
                isGranted = (ActivityCompat.checkSelfPermission(
                    requireActivity(),
                    permission
                )) == PackageManager.PERMISSION_GRANTED
            }
        }
        return isGranted
//        return ((ActivityCompat.checkSelfPermission(requireContext(), CAMERA_PERMISSION)) == PackageManager.PERMISSION_GRANTED
//                && (ActivityCompat.checkSelfPermission(requireContext(), CAMERA_PERMISSION)) == PackageManager.PERMISSION_GRANTED)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            RC_PERMISSION -> {
                var allPermissionsGranted = false
                for (result in grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        allPermissionsGranted = false
                        break
                    } else {
                        allPermissionsGranted = true
                    }
                }
                if (allPermissionsGranted) moveToNextView() else permissionsNotGranted()
            }
        }

    }

    private fun permissionsNotGranted() {
        AlertDialog.Builder(requireContext()).setTitle("Permissions required")
            .setMessage("These permissions are required to use this app. Please allow Camera and Audio permissions first")
            .setCancelable(false)
            .setPositiveButton("Grant") { dialog, which -> requestPermissions() }
            .show()
    }
}