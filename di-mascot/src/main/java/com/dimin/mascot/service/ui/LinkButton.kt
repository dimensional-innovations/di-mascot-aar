package com.dimin.mascot.service.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import com.dimin.mascot.service.Config

class LinkButton: androidx.appcompat.widget.AppCompatButton {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    init {

    }

    override fun dispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)
        setBackgroundColor(Color.TRANSPARENT)
        setTextColor(Config.Colors.primary_color)
        paintFlags = Paint.UNDERLINE_TEXT_FLAG

    }
}