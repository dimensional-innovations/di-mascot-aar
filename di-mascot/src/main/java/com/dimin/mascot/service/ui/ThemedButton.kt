package com.dimin.mascot.service.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import com.dimin.mascot.service.Config


class ThemedButton: androidx.appcompat.widget.AppCompatButton {
    val shape =  GradientDrawable()

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    init {

    }


    override fun dispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)
        shape.cornerRadius = (height / 2).toFloat()
        shape.setColor(Config.Colors.primary_color)
        background = shape

        setTextColor(Color.WHITE)
    }

    fun reset() {
        shape.cornerRadius = (height / 2).toFloat()
        shape.setColor(Config.Colors.primary_color)
        background = shape

        setTextColor(Color.WHITE)
    }

    fun changeColor(color: Int) {
        shape.setColor(color)
        background = shape
    }

}