package com.dimin.mascot.service

import android.content.SharedPreferences
import android.graphics.Color
import androidx.core.content.edit


object Config {

    var userDefaults: SharedPreferences? = null
    object Settings {
        var school: String? = null
        var loaded: String? = null
        var back: String? = null
    }

    object Colors {
        var primary_color: Int = Color.parseColor("#990000")
        var secondary_color: Int = Color.WHITE
    }

    object Images {
        var sponsor_image_url: String? = null
        var sponsor_link_url: String? = null
        var mascot: Mascot? = null
    }

    object Mascot {
        var logo : String? = null
        var background: String? = null
        var holding_phone: String? = null
        var onboarding_pose: String? = null
        var texture: String? = null
        lateinit var gltf: List<String?>
        lateinit var gifs : List<String?>
        lateinit var labels : List<String?>
        var sponsor: String? = null
    }
}

