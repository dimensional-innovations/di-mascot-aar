package com.dimin.mascot

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.fragment.findNavController
import com.dimin.mascot.service.Config
import com.dimin.mascot.service.ui.LinkButton
import com.dimin.mascot.service.ui.ThemedButton
import com.getkeepsafe.relinker.BuildConfig.APPLICATION_ID
import com.google.firebase.BuildConfig.APPLICATION_ID


class OnboardFragment : Fragment() {

    var step:Int = 1
    lateinit var imageView:ImageView
    lateinit var actionButton:ThemedButton
    lateinit var skipButton:LinkButton
    lateinit var testApiButton:ThemedButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_onboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

            //if terms accepted it will navigate to hame fragment
            if (Config.userDefaults?.getBoolean("termsAccepted",false) == true){
                    findNavController().navigate(R.id.action_OnboardFragment_to_HomeFragment)

            } else {
                imageView = view.findViewById(R.id.onboard_background)
                loadStep()
                print("Loaded Onboard")

                actionButton = view.findViewById(R.id.onboard_button)
                actionButton.setOnClickListener {
                    print("Click Onboard")
                    step++
                    loadStep()
                }

                skipButton = view.findViewById(R.id.skip_button)
                skipButton.setOnClickListener {
                    findNavController().navigate(R.id.action_OnboardFragment_to_TermsFragment)
                }
            }
        }

    private fun loadStep() {
        // remove overlay images

        when(step) {
            1 -> {
                imageView.setImageResource(R.drawable.silhouette_bg)
            }
            2 -> {
                imageView.setImageResource(R.drawable.park_bg)
                // draw other images over
            }
            3 -> {
                imageView.setImageResource(R.drawable.holding_phone)
            }
            else -> findNavController().navigate(R.id.action_OnboardFragment_to_TermsFragment)
        }
    }


}