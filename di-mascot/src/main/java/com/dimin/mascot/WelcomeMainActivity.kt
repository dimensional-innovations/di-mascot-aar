package com.dimin.mascot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.dimin.mascot.service.ui.ThemedButton

class WelcomeMainActivity : AppCompatActivity() {


    lateinit var texasSchool: ThemedButton
    lateinit var virginiaSchool: ThemedButton
    lateinit var syracuseSchool: ThemedButton


    lateinit var mLayout: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mLayout = layoutInflater.inflate(R.layout.activity_welcome_main, null)
        setContentView(mLayout)
        setupButtons()
    }

    fun setupButtons() {

        texasSchool = mLayout.findViewById(R.id.texasButton6)
        texasSchool.setOnClickListener {
            println("Texas")
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("school", "Texas")
            startActivity(intent)
            finish()
        }

        virginiaSchool = mLayout.findViewById(R.id.virginiaButton)
        virginiaSchool.setOnClickListener {

            println("Virginia")
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("school", "Virginia")
            startActivity(intent)
            finish()
        }

        syracuseSchool = mLayout.findViewById(R.id.syracuseButton)
        syracuseSchool.setOnClickListener {

            println("Syracuse")
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("school", "Syracuse")
            startActivity(intent)
            finish()
        }
    }
}