package com.dimin.mascot

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.dimin.mascot.service.Config
import com.google.firebase.FirebaseApp
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.R)
    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        Config.userDefaults = getSharedPreferences("Settings", Context.MODE_PRIVATE)

        val b = intent.extras
        if ( b != null) {
            Config.Settings.back = b.getString("back")
            if (b.getString("school") != null){
                println("intents " + b.getString("school"))
                when (b.getString("school")) {
                    "Texas" -> {
                       loadSchoolData("Texas A&M")
                    }
                    "Virginia" -> {
                        loadSchoolData("Virginia Tech")
                  }
                    "Syracuse" -> {
                        loadSchoolData("Syracuse")
                  }
                }
            }
        }

    }

    private fun loadSchoolData(school: String){
        FirebaseApp.initializeApp(this)

        val database = Firebase.database("https://test-9d50b-default-rtdb.firebaseio.com")
        val mDatabase = database.reference

        mDatabase.child(school).get().addOnSuccessListener {
            Log.e("firebase", it.child("school").value.toString())
            Config.Settings.school = it.child("school").value.toString()
            Config.Colors.primary_color = Color.parseColor(it.child("primary_color").value.toString())
            Config.Colors.secondary_color = Color.parseColor(it.child("primary_color").value.toString())
            Config.Images.sponsor_image_url = it.child("sponsor_image_url").value.toString()
            Config.Images.sponsor_link_url = it.child("sponsor_link_url").value.toString()
            Config.Mascot?.logo = it.child("mascot").child("logo").value.toString()
            Config.Mascot?.background = it.child("mascot").child("background").value.toString()
            Config.Mascot?.holding_phone = it.child("mascot").child("holding_phone").value.toString()
            Config.Mascot?.onboarding_pose = it.child("mascot").child("onboarding_pose").value.toString()
            Config.Mascot?.gltf = it.child("mascot").child("gltf").value as List<String?>
            Config.Mascot?.gifs = it.child("mascot").child("gifs").value as List<String?>
            Log.e("firebase", it.child("mascot").child("gifs").value.toString())
            Config.Mascot?.labels = it.child("mascot").child("labels").value as List<String?>
            Config.Mascot?.sponsor = it.child("mascot").child("sponsor").value.toString()
            setContentView(R.layout.activity_main)
        }.addOnFailureListener{
            Log.e("firebase", "Error getting data", it)
        }

    }
}