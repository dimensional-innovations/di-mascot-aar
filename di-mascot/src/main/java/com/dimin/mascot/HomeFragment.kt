package com.dimin.mascot

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment



/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = Intent(activity, ModelMascotActivity::class.java)
        intent.putExtra("isMascot", true)
        activity?.startActivity(intent)
        activity?.finish()
    }
}